using rpg_characters;
using rpg_characters.Character;
using rpg_characters.Items;
using static rpg_characters.Character.CharacterClasses;
using static rpg_characters.Character.Character;

namespace rpg_characters_test
{
    public class AllTests
    {
        public class CharacterTests
        {
            [Theory]
            [InlineData(CharacterClass.Mage)]
            [InlineData(CharacterClass.Ranger)]
            [InlineData(CharacterClass.Rogue)]
            [InlineData(CharacterClass.Warrior)]
            public void CreateCharacter_ShouldReturnLevel1(CharacterClass c)
            {
                // Arrange
                Character? currentCharacter = c switch
                {
                    CharacterClass.Mage => new Mage(),
                    CharacterClass.Ranger => new Ranger(),
                    CharacterClass.Rogue => new Rogue(),
                    CharacterClass.Warrior => new Warrior(),
                    _ => null
                };
                var sut = currentCharacter; // sut = System Under Test
            
                // Act
                var expected = 1;
                var result = sut?.CurrentLevel;
            
                // Assert
                Assert.Equal(expected, result);
            }

            [Fact]
            public void CreateCharacter_LevelUp_ShouldReturnLevel2()
            {
                // Arrange
                var sut = new Mage(); // sut = System Under Test
                sut.LevelUp();
            
                // Act
                var expected = 2;
                var result = sut.CurrentLevel;
            
                // Assert
                Assert.Equal(expected, result);
            }

            [Theory]
            [InlineData(CharacterClass.Mage, Constants.MageBaseIntelligence, Constants.MageBaseDexterity, Constants.MageBaseStrength)]
            [InlineData(CharacterClass.Ranger, Constants.RangerBaseIntelligence, Constants.RangerBaseDexterity, Constants.RangerBaseStrength)]
            [InlineData(CharacterClass.Rogue, Constants.RogueBaseIntelligence, Constants.RogueBaseDexterity, Constants.RogueBaseStrength)]
            [InlineData(CharacterClass.Warrior, Constants.WarriorBaseIntelligence, Constants.WarriorBaseDexterity, Constants.WarriorBaseStrength)]
            public void CreateCharacters_CharacterIsLevel1_ShouldReturnProperBaseAttributes(CharacterClass c, int baseIntelligence, int baseDexterity, int baseStrength)
            {
                // Arrange
                Character? sut = c switch
                {
                    CharacterClass.Mage => new Mage(),
                    CharacterClass.Ranger => new Ranger(),
                    CharacterClass.Rogue => new Rogue(),
                    CharacterClass.Warrior => new Warrior(),
                    _ => null
                };
            
                // Act
                var expectedInt = baseIntelligence;
                var resultInt = sut?.GetBaseAttribute(CharacterAttribute.Intelligence);
                var expectedDex = baseIntelligence;
                var resultDex = sut?.GetBaseAttribute(CharacterAttribute.Intelligence);
                var expectedStr = baseIntelligence;
                var resultStr = sut?.GetBaseAttribute(CharacterAttribute.Intelligence);

                // Assert
                Assert.Equal(expectedInt, resultInt);
                Assert.Equal(expectedDex, resultDex);
                Assert.Equal(expectedStr, resultStr);
            }

            [Theory]
            [InlineData(1, CharacterClass.Mage, Constants.MageBaseIntelligence, Constants.MageIntelligenceIncrease, Constants.MageBaseDexterity, Constants.MageDexterityIncrease, Constants.MageBaseStrength, Constants.MageStrengthIncrease)]
            [InlineData(1, CharacterClass.Ranger, Constants.RangerBaseIntelligence, Constants.RangerIntelligenceIncrease, Constants.RangerBaseDexterity, Constants.RangerDexterityIncrease, Constants.RangerBaseStrength, Constants.RangerStrengthIncrease)]
            [InlineData(1, CharacterClass.Rogue, Constants.RogueBaseIntelligence, Constants.RogueIntelligenceIncrease, Constants.RogueBaseDexterity, Constants.RogueDexterityIncrease, Constants.RogueBaseStrength, Constants.RogueStrengthIncrease)]
            [InlineData(1, CharacterClass.Warrior, Constants.WarriorBaseIntelligence, Constants.WarriorIntelligenceIncrease, Constants.WarriorBaseDexterity, Constants.WarriorDexterityIncrease, Constants.WarriorBaseStrength, Constants.WarriorStrengthIncrease)]
            [InlineData(10, CharacterClass.Mage, Constants.MageBaseIntelligence, Constants.MageIntelligenceIncrease, Constants.MageBaseDexterity, Constants.MageDexterityIncrease, Constants.MageBaseStrength, Constants.MageStrengthIncrease)]
            [InlineData(10, CharacterClass.Ranger, Constants.RangerBaseIntelligence, Constants.RangerIntelligenceIncrease, Constants.RangerBaseDexterity, Constants.RangerDexterityIncrease, Constants.RangerBaseStrength, Constants.RangerStrengthIncrease)]
            [InlineData(10, CharacterClass.Rogue, Constants.RogueBaseIntelligence, Constants.RogueIntelligenceIncrease, Constants.RogueBaseDexterity, Constants.RogueDexterityIncrease, Constants.RogueBaseStrength, Constants.RogueStrengthIncrease)]
            [InlineData(10, CharacterClass.Warrior, Constants.WarriorBaseIntelligence, Constants.WarriorIntelligenceIncrease, Constants.WarriorBaseDexterity, Constants.WarriorDexterityIncrease, Constants.WarriorBaseStrength, Constants.WarriorStrengthIncrease)]
            public void CreateCharacters_LevelUp_ShouldReturnCorrectAttributeValues(int levelUps, CharacterClass c, int baseIntelligence, int intelligenceIncrease, int baseDexterity, int dexterityIncrease, int baseStrength, int strengthIncrease)
            {
                // Arrange
                Character? sut = c switch
                {
                    CharacterClass.Mage => new Mage(),
                    CharacterClass.Ranger => new Ranger(),
                    CharacterClass.Rogue => new Rogue(),
                    CharacterClass.Warrior => new Warrior(),
                    _ => null
                };

                // Act
                sut?.LevelUp(levelUps);
                var resultInt = sut?.GetTotalAttribute(CharacterAttribute.Intelligence);
                var expectedInt = baseIntelligence + (intelligenceIncrease * (sut?.CurrentLevel - 1));
                var resultDex = sut?.GetTotalAttribute(CharacterAttribute.Dexterity);
                var expectedDex = baseDexterity + (dexterityIncrease * (sut?.CurrentLevel - 1));
                var resultStr = sut?.GetTotalAttribute(CharacterAttribute.Strength);
                var expectedStr = baseStrength + (strengthIncrease * (sut?.CurrentLevel - 1));

                // Assert
                Assert.Equal(expectedInt, resultInt);
                Assert.Equal(expectedDex, resultDex);
                Assert.Equal(expectedStr, resultStr);
            }

        }

        public class ItemTests
        {
            [Theory]
            [InlineData(1, 2)]
            [InlineData(10, 15)]
            public void EquipWeapon_LevelCriteriaNotMet_ShouldBeRestricted(int characterLevel, int weaponLevel)
            {
                // Arrange
                var sutWarrior = new Warrior();
                sutWarrior.LevelUp(characterLevel - 1);
                var sutHighLevelWeapon = new Weapon(
                    identifier: 1,
                    name: "TestWeapon",
                    requiredLevel: weaponLevel,
                    weaponType: WeaponType.Axe,
                    baseDamage: 10,
                    attacksPerSecond: 2,
                    itemSlot: ItemSlot.Weapon,
                    attributeBoostType: CharacterAttribute.Strength,
                    attributeBoostValue: 0
                );

                // Act
                var expected = false;
                var result = sutWarrior.EquipWeapon(ItemSlot.Weapon, sutHighLevelWeapon);

                // Assert
                Assert.Equal(expected, result);
            }

            [Theory]
            [InlineData(1, 2)]
            [InlineData(10, 12)]
            public void EquipArmor_LevelCriteriaNotMet_ShouldBeRestricted(int characterLevel, int armorLevel)
            {
                // Arrange
                var sutWarrior = new Warrior();
                sutWarrior.LevelUp(characterLevel - 1);
                var sutHighLevelArmor = new Armor(
                    identifier: 1,
                    name: "TestArmor",
                    requiredLevel: armorLevel,
                    armorType: ArmorType.Plate,
                    itemSlot: ItemSlot.Body,
                    attributeBoostType: CharacterAttribute.Strength,
                    attributeBoostValue: 0
                );

                // Act
                var expected = false;
                var result = sutWarrior.EquipArmor(sutHighLevelArmor);

                // Assert
                Assert.Equal(expected, result);
            }
        
            [Theory]
            [InlineData(CharacterClass.Mage, WeaponType.Axe)]
            [InlineData(CharacterClass.Mage, WeaponType.Bow)]
            [InlineData(CharacterClass.Mage, WeaponType.Dagger)]
            [InlineData(CharacterClass.Mage, WeaponType.Hammer)]
            [InlineData(CharacterClass.Mage, WeaponType.Sword)]
            [InlineData(CharacterClass.Ranger, WeaponType.Dagger)]
            [InlineData(CharacterClass.Rogue, WeaponType.Axe)]
            [InlineData(CharacterClass.Warrior, WeaponType.Bow)]
            public void EquipWeapon_ClassCriteriaNotMet_ShouldBeRestricted(CharacterClass characterClass, WeaponType weaponType)
            {
                // Arrange
                Character sutCharacter = null;
                switch (characterClass)
                {
                    case CharacterClass.Mage:
                    default:
                        sutCharacter = new Mage();
                        break;
                    case CharacterClass.Ranger:
                        sutCharacter = new Ranger();
                        break;
                    case CharacterClass.Rogue:
                        sutCharacter = new Rogue();
                        break;
                    case CharacterClass.Warrior:
                        sutCharacter = new Warrior();
                        break;
                }

                var sutWeapon = new Weapon(
                    identifier: 1,
                    name: "TestBow",
                    requiredLevel: 1,
                    weaponType: weaponType,
                    baseDamage: 10,
                    attacksPerSecond: 2,
                    itemSlot: ItemSlot.Weapon,
                    attributeBoostType: CharacterAttribute.Strength,
                    attributeBoostValue: 0
                );

                // Act
                var expected = false;
                var result = sutCharacter.EquipWeapon(ItemSlot.Weapon, sutWeapon);

                // Assert
                Assert.Equal(expected, result);
            }
        
            [Theory]
            [InlineData(CharacterClass.Warrior, ArmorType.Cloth)]
            [InlineData(CharacterClass.Mage, ArmorType.Leather)]
            [InlineData(CharacterClass.Ranger, ArmorType.Plate)]
            [InlineData(CharacterClass.Rogue, ArmorType.Cloth)]
            public void EquipArmor_ClassCriteriaNotMet_ShouldBeRestricted(CharacterClass characterClass, ArmorType armorType)
            {
                // Arrange
                Character sutCharacter = null;
                switch (characterClass)
                {
                    case CharacterClass.Mage:
                    default:
                        sutCharacter = new Mage();
                        break;
                    case CharacterClass.Ranger:
                        sutCharacter = new Ranger();
                        break;
                    case CharacterClass.Rogue:
                        sutCharacter = new Rogue();
                        break;
                    case CharacterClass.Warrior:
                        sutCharacter = new Warrior();
                        break;
                }

                var sutArmor = new Armor(
                    identifier: 1,
                    name: "TestArmor",
                    requiredLevel: 1,
                    armorType: armorType,
                    itemSlot: ItemSlot.Body,
                    attributeBoostType: CharacterAttribute.Strength,
                    attributeBoostValue: 0
                );

                // Act
                var expected = false;
                var result = sutCharacter.EquipArmor(sutArmor);

                // Assert
                Assert.Equal(expected, result);
            }
        
            [Theory]
            [InlineData(1,1)]
            [InlineData(15,1)]
            [InlineData(15,15)]
            public void EquipWeapon_CriteriaMet_ShouldSucceed(int characterLevel, int weaponLevel)
            {
                // Arrange
                var sutWarrior = new Warrior();
                sutWarrior.LevelUp(characterLevel - 1);
                var sutHighLevelWeapon = new Weapon(
                    identifier: 1,
                    name: "TestWeapon",
                    requiredLevel: weaponLevel,
                    weaponType: WeaponType.Axe,
                    baseDamage: 10,
                    attacksPerSecond: 2,
                    itemSlot: ItemSlot.Weapon,
                    attributeBoostType: CharacterAttribute.Strength,
                    attributeBoostValue: 0
                );

                // Act
                var expected = true;
                var result = sutWarrior.EquipWeapon(ItemSlot.Weapon, sutHighLevelWeapon);

                // Assert
                Assert.Equal(expected, result);
            }
        
            [Theory]
            [InlineData(1,1)]
            [InlineData(15,1)]
            [InlineData(15,15)]
            public void EquipArmor_WithCriteriaMet_ShouldSucceed(int characterLevel, int armorLevel)
            {
                // Arrange
                var sutWarrior = new Warrior();
                sutWarrior.LevelUp(characterLevel - 1);
                var sutHighLevelArmor = new Armor(
                    identifier: 1,
                    name: "TestArmor",
                    requiredLevel: armorLevel,
                    armorType: ArmorType.Plate,
                    itemSlot: ItemSlot.Body,
                    attributeBoostType: CharacterAttribute.Strength,
                    attributeBoostValue: 0
                );

                // Act
                var expected = true;
                var result = sutWarrior.EquipArmor(sutHighLevelArmor);

                // Assert
                Assert.Equal(expected, result);
            }
        
            [Fact]
            public void TotalDamageValue_WhenNoWeaponEquipped_ShouldReturnDefaultDamage()
            {
                // Arrange
                var sutWarrior = new Warrior();
            
                // Act
                var expected = 1.05f;
                var result = sutWarrior.TotalDamageValue();

                // Assert
                Assert.Equal(expected, result);
            }
        
            [Theory]
            [InlineData(7, 1.1f, 8.08f)]
            [InlineData(50, 2.95f, 154.88f)]
            public void TotalDamageValue_WhenWeaponEquipped_ShouldReturnCorrectDamage(int baseDamage, float attacksPerSecond, float expected)
            {
                // Arrange
                var sutWarrior = new Warrior();
                var sutWeapon = new Weapon(
                    identifier: 1,
                    name: "TestAxe",
                    requiredLevel: 1,
                    weaponType: WeaponType.Axe,
                    baseDamage: baseDamage,
                    attacksPerSecond: attacksPerSecond,
                    itemSlot: ItemSlot.Weapon,
                    attributeBoostType: CharacterAttribute.Intelligence,
                    attributeBoostValue: 5
                );
            
                sutWarrior.EquipWeapon(ItemSlot.Weapon, sutWeapon);

                // Act
                var result = sutWarrior.TotalDamageValue();
            
                // Assert
                Assert.Equal(expected, result);
            }
        
            [Theory]
            [InlineData(WeaponType.Axe, 7, 1.1f, ArmorType.Plate, 1, CharacterAttribute.Strength, 8.16f)]
            [InlineData(WeaponType.Hammer, 11, 1.5f, ArmorType.Mail, 17, CharacterAttribute.Strength, 20.13f)]
            [InlineData(WeaponType.Sword, 30, 0.5f, ArmorType.Mail, 10, CharacterAttribute.Dexterity, 15.75f)]
            public void TotalDamageValue_WhenWeaponAndArmorEquipped_ShouldReturnCorrectDamage(WeaponType weaponType, int baseDamage, float attacksPerSecond, ArmorType armorType, int armorPower, CharacterAttribute attributeBoostType, float expectedDamage)
            {
                // Arrange
                var sutWarrior = new Warrior();
                var sutWeapon = new Weapon(
                    identifier: 1,
                    name: "TestAxe",
                    requiredLevel: 1,
                    weaponType: weaponType,
                    baseDamage: baseDamage,
                    attacksPerSecond: attacksPerSecond,
                    itemSlot: ItemSlot.Weapon,
                    attributeBoostType: CharacterAttribute.Strength,
                    attributeBoostValue: 0
                );
            
                var sutArmor = new Armor(
                    identifier: 1,
                    name: "TestArmor",
                    requiredLevel: 1,
                    armorType: armorType,
                    itemSlot: ItemSlot.Body,
                    attributeBoostType: attributeBoostType,
                    attributeBoostValue: armorPower
                );
            
                sutWarrior.EquipWeapon(ItemSlot.Weapon, sutWeapon);
                sutWarrior.EquipArmor(sutArmor);

                // Act
                var resultDamage = sutWarrior.TotalDamageValue();
            
                // Assert
                Assert.Equal(expectedDamage, resultDamage);
            }
        }
    }
}