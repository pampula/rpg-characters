# Description
'RPG Characters' is a C# console app where the user has a roster of four different traditional role-playing game characters: a mage, a ranger, a rogue and a warrior. The user can choose names, equip different items and level up each character separately.

Each character has three base attributes: Intelligence, Dexterity and Strength. Characters deal damage based on their weapon damage and a modifier that is dependent on the character class. Mage gets damage boost from Intelligence, Ranger and Rogue get damage boost from Dexterity and Warrior gets damage boost from Strength.

Characters can increase their base attributes by either levelling up or by equipping items that have attribute boosts.

Items are divided into two categories: armor and weapons.

Weapons have multiple subcategories (e.g. Staff, Sword, Dagger) and each character can wield one weapon at a time. Each weapon is restricted to certain character classes, e.g only a mage can equip a staff and only a ranger can equip a bow.

Armor pieces have multiple subcategories (e.g. Cloth, Plate) and each armor piece also has a item slot where they belong to (e.g. Head, Body). A character can wear multiple armor pieces at the same time but only one per item slot (e.g. only one armor piece in Head slot). Each armor piece is restricted to certain character classes, e.g. only a mage can equip a cloth armor piece and only a warrior can equip a plate armor piece.

## Features
-   Choose a name for each character. Name can be either written manually or generated randomly.
-   Check character attributes and level up character.
-   Check character equipment and equip new items (weapons or armor).
-   See list of available weapons and armor.
-   Regenerate new list of available weapons or armor.
-   Credits page
-   Change font color
 

## Contributors
- Tuomo Kuusisto https://gitlab.com/pampula


## License
[MIT](https://choosealicense.com/licenses/mit/)
