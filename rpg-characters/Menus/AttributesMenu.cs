﻿using rpg_characters.Character;

namespace rpg_characters.Menus
{
    public class AttributesMenu : MenuBase
    {
        public override void ShowMenu()
        {
            sb.Clear();
            sb.Append("Attributes\n\n");
        
            ShowCharacterSelect((result) =>
            {
                if (result == true)
                {
                    ShowAttributes();
                }
                else
                {
                    ReturnToParentMenu();
                }
            });
        }

        /// <summary>
        /// Generate info for selected character show page navigation instructions.
        /// </summary>
        void ShowAttributes()
        {
            Console.Clear();
            sb.Clear();

            Character.Character c = CharacterManager.SelectedCharacter;
            sb.Append($"{CharacterManager.GetCharacterInfoShort()}\n\n" +
                      $"Strength: {c.GetTotalAttribute(CharacterAttribute.Strength)} " +
                      $"{(c.GetAttributeBoost(CharacterAttribute.Strength) > 0 ? $" (+{c.GetAttributeBoost(CharacterAttribute.Strength)} from items)" : "")}" +
                      $"\n" +
                      $"Dexterity: {c.GetTotalAttribute(CharacterAttribute.Dexterity)}" +
                      $"{(c.GetAttributeBoost(CharacterAttribute.Dexterity) > 0 ? $" (+{c.GetAttributeBoost(CharacterAttribute.Dexterity)} from items)" : "")}" +
                      $"\n" +
                      $"Intelligence: {c.GetTotalAttribute(CharacterAttribute.Intelligence)}" +
                      $"{(c.GetAttributeBoost(CharacterAttribute.Intelligence) > 0 ? $" (+{c.GetAttributeBoost(CharacterAttribute.Intelligence)} from items)" : "")}" +
                      $"\n" +
                      $"Damage: {c.TotalDamage()}\n\n" +
                      $"1.Level up\n2.Level up x 10\nArrow keys: Switch character\n\nEsc. Return");
        
            Console.WriteLine(sb.ToString());

            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        selected = true;
                        Console.Clear();
                        CharacterManager.SelectedCharacter.LevelUp();
                        Console.WriteLine($"{CharacterManager.SelectedCharacter.Name} reached level {CharacterManager.SelectedCharacter.CurrentLevel}!");
                        ShowAttributes();
                        break;
                    case ConsoleKey.D2:
                        selected = true;
                        Console.Clear();
                        CharacterManager.SelectedCharacter.LevelUp(15);
                        Console.WriteLine($"{CharacterManager.SelectedCharacter.Name} reached level {CharacterManager.SelectedCharacter.CurrentLevel}!");
                        ShowAttributes();
                        break;
                    case ConsoleKey.LeftArrow:
                    case ConsoleKey.DownArrow:
                        selected = true;
                        CharacterManager.PreviousCharacter();
                        ShowAttributes();
                        break;
                    case ConsoleKey.RightArrow:
                    case ConsoleKey.UpArrow:
                        selected = true;
                        CharacterManager.NextCharacter();
                        ShowAttributes();
                        break;
                    case ConsoleKey.Escape:
                        selected = true;
                        break;
                }
            }
        }

        public override void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}