﻿using System.Text;

namespace rpg_characters.Menus
{
    public static class MenuManager
    {
        private static MenuType CurrentMenu = MenuType.MainMenu;
        private static readonly MainMenu MainMenu = new MainMenu();
        private static readonly AttributesMenu AttributesMenu = new AttributesMenu();
        private static readonly EquipmentMenu EquipmentMenu = new EquipmentMenu();
        private static readonly CreditsMenu CreditsMenu = new CreditsMenu();
        private static StringBuilder sb = new StringBuilder();
        private static ConsoleColor _currentColor = ConsoleColor.DarkYellow;

        /// <summary>
        /// Shows a specific menu based on CurrentMenu.
        /// </summary>
        public static void ShowMenus()
        {
            Console.Clear();
            while (true)
            {
                if (CurrentMenu == MenuType.MainMenu)
                {
                    MainMenu.ShowMenu();
                }
                else if (CurrentMenu == MenuType.AttributesMenu)
                {
                    AttributesMenu.ShowMenu();
                }
                else if (CurrentMenu == MenuType.EquipmentMenu)
                {
                    EquipmentMenu.ShowMenu();
                }
                else if (CurrentMenu == MenuType.CreditsMenu)
                {
                    CreditsMenu.ShowMenu();
                }
            }
        }

        /// <summary>
        /// Sets CurrentMenu
        /// </summary>
        /// <param name="newMenu">The next menu that will be shown.</param>
        public static void SetMenu(MenuType newMenu)
        {
            CurrentMenu = newMenu;
        }
        
        /// <summary>
        /// Change font color
        /// </summary>
        public static void ChangeColor()
        {
            for (int i = 0; i < Constants.ForegroundColors.Count; ++i)
            {
                if (_currentColor == Constants.ForegroundColors[i])
                {
                    _currentColor = i == Constants.ForegroundColors.Count - 1
                        ? Constants.ForegroundColors[0]
                        : Constants.ForegroundColors[i + 1];
                    Console.ForegroundColor = _currentColor;
                    break;
                }
            }
        }
    }
}