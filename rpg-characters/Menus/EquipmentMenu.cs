﻿using System.Text;
using rpg_characters.Character;
using rpg_characters.Items;

namespace rpg_characters.Menus
{
    public class EquipmentMenu : MenuBase
    {
        public override void ShowMenu()
        {
            sb.Clear();
            sb.Append("Equipment\n\n");

            ShowCharacterSelect((result) =>
            {
                if (result == true)
                {
                    ShowInventoryScreen();
                }
                else
                {
                    ReturnToParentMenu();
                }
            });
        }

        /// <summary>
        /// Generate inventory screen for selected character and show page navigation instructions.
        /// </summary>
        void ShowInventoryScreen()
        {
            Console.Clear();
            ShowEquippedItems();
            Console.WriteLine("1.Change weapon\n2.Change armor\nArrow keys: Switch character\n\nEsc. Return");

            bool selected = false;

            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        ShowEquipScreen(ItemType.Weapon);
                        selected = true;
                        break;
                    case ConsoleKey.D2:
                        ShowEquipScreen(ItemType.Armor);
                        selected = true;
                        break;
                    case ConsoleKey.LeftArrow:
                    case ConsoleKey.DownArrow:
                        CharacterManager.PreviousCharacter();
                        ShowInventoryScreen();
                        selected = true;
                        break;
                    case ConsoleKey.RightArrow:
                    case ConsoleKey.UpArrow:
                        CharacterManager.NextCharacter();
                        ShowInventoryScreen();
                        selected = true;
                        break;
                    case ConsoleKey.Escape:
                        selected = true;
                        break;
                }
            }
        }

        /// <summary>
        /// Generate list of equipped pieces of armor and weapon.
        /// </summary>
        void ShowEquippedItems()
        {
            var sb = new StringBuilder();
            sb.Append($"{CharacterManager.GetCharacterInfoShort()}\n\n");

            if (CharacterManager.SelectedCharacter.EquippedArmors.Count > 0 ||
                CharacterManager.SelectedCharacter.EquippedWeapons.Count > 0)
            {
                foreach (var kvp in CharacterManager.SelectedCharacter.EquippedArmors)
                {
                    sb.Append($"{kvp.Value.GetFullName()}\n");
                }

                foreach (var kvp in CharacterManager.SelectedCharacter.EquippedWeapons)
                {
                    sb.Append($"{kvp.Value.GetFullName()}\n");
                }
            }
            else
            {
                sb.Append("No items equipped.\n");
            }

            Console.WriteLine(sb.ToString());
        }

        /// <summary>
        /// Show list of available armor pieces or weapons.
        /// </summary>
        /// <param name="itemType">Item type to show.</param>
        void ShowEquipScreen(ItemType itemType)
        {
            Console.Clear();
            var sb = new StringBuilder();
            sb.Append($"{CharacterManager.GetCharacterInfoShort()}\n\n" +
                      $"{GetEquippableItems(itemType)}\n\n" +
                      $"1.Equip {(itemType == ItemType.Weapon ? "weapon" : "armor")}\n{(itemType == ItemType.Weapon ? "2.Generate new list of weapons" : "2.Generate new list of armor")}\nArrow keys: Switch character\n\nEsc. Return");
            Console.WriteLine(sb.ToString());

            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        selected = true;
                        switch (itemType)
                        {
                            case ItemType.Weapon:
                                EquipWeapon();
                                break;
                            case ItemType.Armor:
                                EquipArmor();
                                break;
                        }

                        break;
                    case ConsoleKey.D2:
                        ItemManager.GenerateItems(itemType);
                        ShowEquipScreen(itemType);
                        selected = true;
                        break;
                    case ConsoleKey.LeftArrow:
                    case ConsoleKey.DownArrow:
                        CharacterManager.PreviousCharacter();
                        ShowEquipScreen(itemType);
                        selected = true;
                        break;
                    case ConsoleKey.RightArrow:
                    case ConsoleKey.UpArrow:
                        CharacterManager.NextCharacter();
                        ShowEquipScreen(itemType);
                        selected = true;
                        break;
                    case ConsoleKey.Escape:
                        ShowInventoryScreen();
                        selected = true;
                        break;
                }
            }
        }

        /// <summary>
        /// Generates a list of items that are available.
        /// </summary>
        /// <param name="itemType">Type of items to show.</param>
        /// <returns>A string of equippable items.</returns>
        public string GetEquippableItems(ItemType itemType)
        {
            var sb = new StringBuilder();

            switch (itemType)
            {
                case ItemType.Weapon:

                    if (ItemManager.AvailableWeapons.Count == 0)
                    {
                        sb.Append("No weapons available.");
                    }
                    else
                    {
                        sb.Append(String.Format("{0,-3}", "#"));
                        sb.Append(String.Format(" {0,-33}|", ("WEAPON NAME")));
                        sb.Append(String.Format(" {0,-10}|", ("LEVEL REQ.")));
                        sb.Append(String.Format(" {0,-48}|", ("DAMAGE")));
                        sb.Append(String.Format(" {0,-18}|", ("ATTRIBUTE BOOST")));
                        sb.Append(String.Format(" {0,-32}", ("RESTRICTIONS")));
                        sb.Append("\n");

                        for (int i = 0; i < ItemManager.AvailableWeapons.Count; ++i)
                        {
                            Weapon w = ItemManager.AvailableWeapons[i];
                            string restriction = "";
                            bool canEquip = true;
                            try
                            {
                                canEquip = CharacterManager.SelectedCharacter.CanEquipWeapon(w);
                            }
                            catch (RpgExceptions.InvalidWeaponException e)
                            {
                                restriction = $"Requires {ItemManager.GetCharacterClassesForItem(w.WeaponType)}.";
                                canEquip = false;
                            }
                            catch (Exception e)
                            {

                                restriction = e.Message;
                                canEquip = false;
                            }
                            
                            sb.Append(String.Format("{0,-3}", (i+1)));
                            sb.Append(String.Format(" {0,-33}|", (w.GetFullName())));
                            sb.Append(String.Format(" {0,-10}|", (w.RequiredLevel)));
                            sb.Append(String.Format(" Damage: {0,-5}", (w.BaseDamage)));
                            sb.Append(String.Format(" Attacks per second: {0} =>", (w.AttacksPerSecond)));
                            sb.Append(String.Format(" DPS: {0,-3} |", (w.DPS)));
                            sb.Append(String.Format(" {0,-18}|", (w.AttributeBoostValue > 0 ? $"+{w.AttributeBoostValue} {w.AttributeBoostType}" : "")));
                            if(!canEquip)sb.Append(String.Format(" {0,-32}", (restriction)));
                            sb.Append("\n");
                        }
                    }

                    break;
                case ItemType.Armor:
                    if (ItemManager.AvailableArmors.Count == 0)
                    {
                        sb.Append("No armor available.\n");
                    }
                    else
                    {
                        int longestNameLength = ItemManager.GetLongestName(ItemType.Armor);
                        
                        sb.Append(String.Format("{0,-3}", "#"));
                        if (longestNameLength < 40)
                        {
                            sb.Append(String.Format(" {0,-40}|", ("ARMOR NAME")));
                        }
                        else
                        {
                            sb.Append(String.Format(" {0,-50}|", ("ARMOR NAME")));
                        }
                        sb.Append(String.Format(" {0,-10}|", ("LEVEL REQ.")));
                        sb.Append(String.Format(" {0,-18}|", ("ATTRIBUTE BOOST")));
                        sb.Append(String.Format(" {0,-32}", ("RESTRICTIONS")));
                        sb.Append("\n");
                        for (int i = 0; i < ItemManager.AvailableArmors.Count; ++i)
                        {
                            Armor a = ItemManager.AvailableArmors[i];
                            string restriction = "";

                            bool canEquip = true;
                            try
                            {
                                canEquip = CharacterManager.SelectedCharacter.CanEquipArmor(a);
                            }
                            catch (RpgExceptions.InvalidArmorException e)
                            {
                                restriction = $"Requires {ItemManager.GetCharacterClassesForItem(a.ArmorType)}.";
                                canEquip = false;
                            }
                            catch (Exception e)
                            {
                                restriction = e.Message;
                                canEquip = false;
                            }

                            sb.Append(String.Format("{0,-3}", (i+1)));
                            if (longestNameLength < 40)
                            {
                                sb.Append(String.Format(" {0,-40}|", (a.GetFullName())));
                            }
                            else
                            {
                                sb.Append(String.Format(" {0,-50}|", (a.GetFullName())));
                            }
                            sb.Append(String.Format(" {0,-10}|", (a.RequiredLevel)));
                            sb.Append(String.Format(" {0,-18}|", (a.AttributeBoostValue > 0 ? $"+{a.AttributeBoostValue} {a.AttributeBoostType}" : "")));
                            if(!canEquip)sb.Append(String.Format(" {0,-32}", (restriction)));
                            sb.Append("\n");
                        }
                    }

                    break;
            }

            return sb.ToString();
        }

        /// <summary>
        /// Show armor selection prompt screen.
        /// </summary>
        void EquipArmor()
        {
            Console.WriteLine("Which armor do you want to equip?");
            bool valid = int.TryParse(Console.ReadLine(), out int index);

            if (valid && index > 0 && index <= ItemManager.AvailableArmors.Count)
            {
                Armor a = ItemManager.AvailableArmors[index - 1];
                ItemManager.EquipArmor(a, CharacterManager.SelectedCharacter);
                Thread.Sleep(TimeSpan.FromSeconds(Constants.DialogueDelay));
            }
            else
            {
                Console.WriteLine("Input is not valid.");
                Thread.Sleep(TimeSpan.FromSeconds(Constants.DialogueDelay));
            }

            ShowEquipScreen(ItemType.Armor);
        }

        /// <summary>
        /// Show weapon selection prompt screen.
        /// </summary>
        void EquipWeapon()
        {
            var sb = new StringBuilder();
            sb.Append("Which weapon do you want to equip?");
            Console.WriteLine(sb.ToString());
            sb.Clear();
            bool valid = int.TryParse(Console.ReadLine(), out int index);

            if (valid && index > 0 && index <= ItemManager.AvailableWeapons.Count)
            {
                Weapon w = ItemManager.AvailableWeapons[index - 1];
                ItemManager.EquipWeapon(w, CharacterManager.SelectedCharacter);
                Thread.Sleep(TimeSpan.FromSeconds(Constants.DialogueDelay));
            }
            else
            {
                Console.WriteLine("Input is not valid.");
                Thread.Sleep(TimeSpan.FromSeconds(Constants.DialogueDelay));
            }

            ShowEquipScreen(ItemType.Weapon);
        }

        /// <summary>
        /// Return to main menu.
        /// </summary>
        public override void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}