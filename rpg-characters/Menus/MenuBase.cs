﻿using System.Text;
using rpg_characters.Character;

namespace rpg_characters.Menus
{
    public abstract class MenuBase
    {
        private protected StringBuilder sb = new StringBuilder();
        public abstract void ShowMenu();

        public abstract void ReturnToParentMenu();

        /// <summary>
        /// Show the character selection prompt.
        /// </summary>
        /// <param name="result">Action to trigger based on character selection.</param>
        protected void ShowCharacterSelect(Action<bool> result)
        {
            Console.Clear();

            for (int i = 0; i < CharacterManager.CurrentCharacters.Count; ++i)
            {
                sb.Append($"{i+1}. {CharacterManager.CurrentCharacters[i].Name} (Level {CharacterManager.CurrentCharacters[i].CurrentLevel} {CharacterManager.CurrentCharacters[i].CurrentClass})\n");
            }

            sb.Append("\nEsc. Return");
            Console.WriteLine(sb.ToString());

            CharacterManager.SelectedCharacter = null;
            bool selected = false;
        
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        CharacterManager.SelectedCharacter = CharacterManager.CurrentCharacters[0];
                        selected = true;
                        break;
                    case ConsoleKey.D2:
                        CharacterManager.SelectedCharacter = CharacterManager.CurrentCharacters[1];
                        selected = true;
                        break;
                    case ConsoleKey.D3:
                        CharacterManager.SelectedCharacter = CharacterManager.CurrentCharacters[2];
                        selected = true;
                        break;
                    case ConsoleKey.D4:
                        CharacterManager.SelectedCharacter = CharacterManager.CurrentCharacters[3];
                        selected = true;
                        break;
                    case ConsoleKey.Escape:
                        CharacterManager.SelectedCharacter = null;
                        selected = true;
                        break;
                }
            }
        
            result.Invoke(CharacterManager.SelectedCharacter != null);
        }
    }
}