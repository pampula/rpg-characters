﻿namespace rpg_characters.Menus
{
    /// <summary>
    /// List of available menu types
    /// </summary>
    public enum MenuType
    {
        MainMenu,
        AttributesMenu,
        EquipmentMenu,
        CreditsMenu
    }
}
