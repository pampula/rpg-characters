﻿namespace rpg_characters.Menus
{
    public class MainMenu : MenuBase
    {
        /// <summary>
        /// Show a specific menu based on the user input.
        /// </summary>
        public override void ShowMenu()
        {
            Console.Clear();
            Console.WriteLine("Select menu:\n\n1.Attributes\n2.Equipment\n3.Credits\n4.Change font color\n\nQuit game: Q");
            switch (Console.ReadKey(true).Key)
            {
                case ConsoleKey.D1:
                    Console.WriteLine("Show attributes");
                    MenuManager.SetMenu(MenuType.AttributesMenu);
                    break;
                case ConsoleKey.D2:
                    Console.WriteLine("Show equipment");
                    MenuManager.SetMenu(MenuType.EquipmentMenu);
                    break;
                case ConsoleKey.D3:
                    MenuManager.SetMenu(MenuType.CreditsMenu);
                    break;
                case ConsoleKey.D4:
                    MenuManager.ChangeColor();
                    break;
                case ConsoleKey.Q:
                    ReturnToParentMenu();
                    break;
            }
        }



        /// <summary>
        /// Quit game.
        /// </summary>
        public override void ReturnToParentMenu()
        {
            Console.WriteLine("Quitting game...");
            Environment.Exit(0);
        }
    }
}