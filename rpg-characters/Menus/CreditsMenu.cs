﻿namespace rpg_characters.Menus
{
    public class CreditsMenu : MenuBase
    {
        private const int WriteDelay = 3;
        const string CreditsText = "Credits:\n\n" +
                             "Tuomo Kuusisto\n" +
                             "https://gitlab.com/pampula\n\n" +
                             "Project repository: https://gitlab.com/pampula/rpg-characters\n\n" +
                             "Created for Noroff Accelerate C# Fundamentals course on September 2022.\n\nEsc. Return";
        
        /// <summary>
        /// Generate credits text.
        /// </summary>
        public override void ShowMenu()
        {
            Console.Clear();
            var ts = new CancellationTokenSource();
            CancellationToken ct = ts.Token;
            var task1 = Task.Factory.StartNew(() =>
            {
                bool writing = true;

                while (writing)
                {
                    for (int i = 0; i < CreditsText.Length; ++i)
                    {
                        if (writing && !ct.IsCancellationRequested)
                        {
                            Console.Write(CreditsText[i]);
                            Thread.Sleep(WriteDelay);
                        }
                    }

                    writing = false;
                }
            }, ct);

            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        selected = true;
                        ts.Cancel();
                        ReturnToParentMenu();
                        break;
                }
            }
        }

        public override void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}