﻿using System.Drawing;

namespace rpg_characters
{
    public class Constants
    {
        public const float DialogueDelay = 1.5f;
        
        #region CharacterClassStats
        public const int MageBaseStrength = 1;
        public const int MageBaseDexterity = 1;
        public const int MageBaseIntelligence = 8;
        public const int MageStrengthIncrease = 1;
        public const int MageDexterityIncrease = 1;
        public const int MageIntelligenceIncrease = 5;

        public const int RangerBaseStrength = 1;
        public const int RangerBaseDexterity = 7;
        public const int RangerBaseIntelligence = 1;
        public const int RangerStrengthIncrease = 1;
        public const int RangerDexterityIncrease = 5;
        public const int RangerIntelligenceIncrease = 1;
        
        public const int RogueBaseStrength = 2;
        public const int RogueBaseDexterity = 6;
        public const int RogueBaseIntelligence = 1;
        public const int RogueStrengthIncrease = 1;
        public const int RogueDexterityIncrease = 4;
        public const int RogueIntelligenceIncrease = 1;
        
        public const int WarriorBaseStrength = 5;
        public const int WarriorBaseDexterity = 2;
        public const int WarriorBaseIntelligence = 1;
        public const int WarriorStrengthIncrease = 3;
        public const int WarriorDexterityIncrease = 2;
        public const int WarriorIntelligenceIncrease = 1;
        #endregion
        
        public static readonly string[] MageNames = { "fire", "spark", "frost", "thunder", "flame", "wind", "dark", "sun", "moon", "dragon" };
        public static readonly string[] RangerNames = { "hunt", "trail", "hide", "stalker", "moss", "lichen", "scent", "bark", "rock", "snare" };
        public static readonly string[] RogueNames = { "poison", "trick", "dagger", "death", "spiral", "shade", "quick", "sharp", "hood", "cloak" };
        public static readonly string[] WarriorNames = { "bone", "crush", "steel", "iron", "strong", "blood", "brave", "horn", "heart", "skull" };

        public const int ItemCommonMaxLevel = 4;
        public const int ItemRareMaxLevel = 8;
        public const int ItemEpicMaxLevel = 12;

        public static readonly List<ConsoleColor> ForegroundColors = new List<ConsoleColor> { ConsoleColor.DarkBlue, ConsoleColor.DarkCyan, ConsoleColor.DarkGreen, ConsoleColor.DarkMagenta, ConsoleColor.DarkRed, ConsoleColor.DarkGray, ConsoleColor.DarkYellow };
    }
}