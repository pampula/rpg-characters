﻿using rpg_characters.Items;

namespace rpg_characters.Character
{
    public abstract class Character
    {
        public CharacterClass CurrentClass { get; protected init; }
        public int CurrentLevel { get; private set; } = 1;

        public void LevelUp(int amount = 1) { CurrentLevel += amount; }
        protected abstract List<WeaponType> AllowedWeaponTypes { get; set; }
        protected abstract List<ArmorType> AllowedArmorTypes { get; set; }
        public Dictionary<ItemSlot, Armor> EquippedArmors { get; private set; } = new Dictionary<ItemSlot, Armor>();
        public Dictionary<ItemSlot, Weapon> EquippedWeapons { get; private set; } = new Dictionary<ItemSlot, Weapon>();

        /// <summary>
        /// Display name of the character
        /// </summary>
        public string Name { get; set; } = "";

        /// <summary>
        /// A character class specific greeting.
        /// </summary>
        public abstract void Greet();

        /// <summary>
        /// A method for equipping a new armor piece. Checks for restrictions before equipping the armor and then removes the old armor before adding the new one.
        /// </summary>
        /// <param name="armor">The armor that is being equipped.</param>
        /// <returns>A boolean based on whether the equip was successful or not.</returns>
        public bool EquipArmor(Armor armor)
        {
            try
            {
                if (CanEquipArmor(armor))
                {
                    if (EquippedArmors.ContainsKey(armor.Slot))
                    {
                        Console.WriteLine(
                            $"Removing {EquippedArmors.GetValueOrDefault(armor.Slot)?.GetFullName()} from slot {armor.Slot}.");
                        EquippedArmors.Remove(armor.Slot);
                    }

                    EquippedArmors.Add(armor.Slot, armor);

                    Console.WriteLine(
                        $"Adding {EquippedArmors.GetValueOrDefault(armor.Slot)?.GetFullName()} to slot {armor.Slot}.");
                    return true;
                }
            }
            catch(Exception e)
            {
                return false;
            }

            return false;
        }
        
        /// <summary>
        /// A method for checking if an armor is equippable or not. Throws InvalidArmorException if the armor is not suitable for this class
        /// and InvalidItemLevelException if the armor is level requirement is too high.
        /// </summary>
        /// <param name="armor">The armor that is being checked.</param>
        /// <returns>A boolean based on whether equipping the armor is possible or not.</returns>
        public bool CanEquipArmor(Armor armor)
        {
            if (!AllowedArmorTypes.Contains(armor.ArmorType))
            {
                throw new RpgExceptions.InvalidArmorException();
            }
            else if (armor.RequiredLevel > CurrentLevel)
            {
                throw new RpgExceptions.InvalidItemLevelException();
            }
            else
            {
                return true;
            }
        }
        
        /// <summary>
        /// A method for equipping a new weapon. Checks for restrictions before equipping the weapon and then removes the old weapon before adding the new one.
        /// </summary>
        /// <param name="weapon">The armor that is being equipped.</param>
        /// <param name="itemSlot">The item slot where the weapon is equipped to.</param>
        /// <returns>A boolean based on whether the equip was successful or not.</returns>
        public bool EquipWeapon(ItemSlot itemSlot, Weapon weapon)
        {
            try
            {
                if (CanEquipWeapon(weapon))
                {
                    if (EquippedWeapons.ContainsKey(itemSlot))
                    {
                        Console.WriteLine(
                            $"Removing {EquippedWeapons.GetValueOrDefault(itemSlot)?.GetFullName()} from slot {itemSlot}.");
                        EquippedWeapons.Remove(itemSlot);
                    }

                    EquippedWeapons.Add(itemSlot, weapon);
                    Console.WriteLine(
                        $"Adding {EquippedWeapons.GetValueOrDefault(itemSlot)?.GetFullName()} to slot {itemSlot}.");
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// A method for checking if an weapon is equippable or not. Throws InvalidWeaponException if the weapon is not suitable for this class
        /// and InvalidItemLevelException if the weapon is level requirement is too high.
        /// </summary>
        /// <param name="weapon">The weapon that is being checked.</param>
        /// <returns>A boolean based on whether equipping the weapon is possible or not.</returns>
        public bool CanEquipWeapon(Weapon weapon)
        {
            if (!AllowedWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new RpgExceptions.InvalidWeaponException();
            }
            else if (weapon.RequiredLevel > CurrentLevel)
            {
                throw new RpgExceptions.InvalidItemLevelException();
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Displays the total damage the character deals and a breakdown of how the damage is counted as a string value. The total damage is a combination of weapon damage and
        /// damage multiplier that is based on the character attributes.
        /// </summary>
        /// <returns>The total damage breakdown as a string value.</returns>
        public string TotalDamage()
        {
            float totalDamage = 0;
            float dps = 0;
            foreach (var kvp in EquippedWeapons)
            {
                dps += kvp.Value.DPS;
            }

            dps = dps < 1 ? 1 : dps;
            float attributeModifier = 1;
            
            switch (CurrentClass)
            {
                case CharacterClass.Mage:
                    attributeModifier = (100 + GetTotalAttribute(CharacterAttribute.Intelligence));
                    break;
                case CharacterClass.Ranger:
                case CharacterClass.Rogue:
                    attributeModifier = (100 + GetTotalAttribute(CharacterAttribute.Dexterity));
                    break;
                case CharacterClass.Warrior:
                    attributeModifier = (100 + GetTotalAttribute(CharacterAttribute.Strength));
                    break;
            }

            totalDamage = (dps * attributeModifier) / (float)100;
            return totalDamage == 1 ? "1" : $"{totalDamage.ToString("0.00")} ({dps.ToString("0.00")} * {(attributeModifier/100.0).ToString("0.00")})";
        }
        
        /// <summary>
        /// Displays the total damage the character deals as a flaot value. The total damage is a combination of weapon damage and
        /// damage multiplier that is based on the character attributes.
        /// </summary>
        /// <returns>The total damage.</returns>
        public float TotalDamageValue()
        {
            float totalDamage = 0;
            float dps = 0;
            foreach (var kvp in EquippedWeapons)
            {
                dps += kvp.Value.DPS;
            }

            dps = dps < 1 ? 1 : dps;
            float attributeModifier = 1;
            
            switch (CurrentClass)
            {
                case CharacterClass.Mage:
                    attributeModifier = (100 + GetTotalAttribute(CharacterAttribute.Intelligence));
                    break;
                case CharacterClass.Ranger:
                case CharacterClass.Rogue:
                    attributeModifier = (100 + GetTotalAttribute(CharacterAttribute.Dexterity));
                    break;
                case CharacterClass.Warrior:
                    attributeModifier = (100 + GetTotalAttribute(CharacterAttribute.Strength));
                    break;
            }
            
            totalDamage = (dps * attributeModifier) / (float)100;
            return MathF.Round(totalDamage * 100f) / 100f;
        }
        
        /// <summary>
        /// The total attribute boost the character currently receives from all the equipped armor pieces and the weapon that boost a certain attribute.
        /// </summary>
        /// <param name="selectedAttribute">The character attribute.</param>
        /// <returns>The total boost amount as an integer.</returns>
        public int GetAttributeBoost(CharacterAttribute selectedAttribute)
        {
            int? armorBoost = EquippedArmors.Where(armor => armor.Value.AttributeBoostType == selectedAttribute)
                .Sum(armor => armor.Value.AttributeBoostValue);
            
            int? weaponBoost = EquippedWeapons.Where(weapon => weapon.Value.AttributeBoostType == selectedAttribute)
                .Sum(weapon => weapon.Value.AttributeBoostValue);

            var totalArmor = armorBoost ?? 0;
            var totalWeapon = weaponBoost ?? 0;

            return totalArmor + totalWeapon;
        }
        
        /// <summary>
        /// The base value for a specific attribute. Base value is the starting value for the attribute on character level 1.
        /// </summary>
        /// <param name="attribute">The character attribute.</param>
        /// <returns>The base attribute as integer value.</returns>
        public int GetBaseAttribute(CharacterAttribute attribute)
        {
            switch (CurrentClass)
            {
                case CharacterClass.Mage:
                    return attribute switch
                    {
                        CharacterAttribute.Strength => Constants.MageBaseStrength,
                        CharacterAttribute.Dexterity => Constants.MageBaseDexterity,
                        CharacterAttribute.Intelligence => Constants.MageBaseIntelligence,
                        _ => 0
                    };
                case CharacterClass.Ranger:
                    return attribute switch
                    {
                        CharacterAttribute.Strength => Constants.RangerBaseStrength,
                        CharacterAttribute.Dexterity => Constants.RangerBaseDexterity,
                        CharacterAttribute.Intelligence => Constants.RangerBaseIntelligence,
                        _ => 0
                    };
                case CharacterClass.Rogue:
                    return attribute switch
                    {
                        CharacterAttribute.Strength => Constants.RogueBaseStrength,
                        CharacterAttribute.Dexterity => Constants.RogueBaseDexterity,
                        CharacterAttribute.Intelligence => Constants.RogueBaseIntelligence,
                        _ => 0
                    };
                case CharacterClass.Warrior:
                    return attribute switch
                    {
                        CharacterAttribute.Strength => Constants.WarriorBaseStrength,
                        CharacterAttribute.Dexterity => Constants.WarriorBaseDexterity,
                        CharacterAttribute.Intelligence => Constants.WarriorBaseIntelligence,
                        _ => 0
                    };
                default:
                    return 0;
            }
        }
        
        /// <summary>
        /// The total value for a specific attribute. Calculated by adding up base attribute with character class specific attribute boost value
        /// gained per each additional character level.
        /// </summary>
        /// <param name="attribute">The character attribute.</param>
        /// <returns>The total attribute as integer value.</returns>
        public int GetTotalAttribute(CharacterAttribute attribute)
        {
            return (CurrentClass, attribute) switch
            {
                (CharacterClass.Mage, CharacterAttribute.Intelligence) => 
                    GetBaseAttribute(CharacterAttribute.Intelligence)
                    + (CurrentLevel - 1) * Constants.MageIntelligenceIncrease
                    + GetAttributeBoost(CharacterAttribute.Intelligence),
                (CharacterClass.Mage, CharacterAttribute.Dexterity) => 
                    GetBaseAttribute(CharacterAttribute.Dexterity)
                    + (CurrentLevel - 1) * Constants.MageDexterityIncrease
                    + GetAttributeBoost(CharacterAttribute.Dexterity),
                (CharacterClass.Mage, CharacterAttribute.Strength) => 
                    GetBaseAttribute(CharacterAttribute.Strength)
                    + (CurrentLevel - 1) * Constants.MageStrengthIncrease
                    + GetAttributeBoost(CharacterAttribute.Strength),
                
                (CharacterClass.Ranger, CharacterAttribute.Intelligence) => 
                    GetBaseAttribute(CharacterAttribute.Intelligence)
                    + (CurrentLevel - 1) * Constants.RangerIntelligenceIncrease
                    + GetAttributeBoost(CharacterAttribute.Intelligence),
                (CharacterClass.Ranger, CharacterAttribute.Dexterity) => 
                    GetBaseAttribute(CharacterAttribute.Dexterity)
                    + (CurrentLevel - 1) * Constants.RangerDexterityIncrease
                    + GetAttributeBoost(CharacterAttribute.Dexterity),
                (CharacterClass.Ranger, CharacterAttribute.Strength) => 
                    GetBaseAttribute(CharacterAttribute.Strength)
                    + (CurrentLevel - 1) * Constants.RangerStrengthIncrease
                    + GetAttributeBoost(CharacterAttribute.Strength),
                
                (CharacterClass.Rogue, CharacterAttribute.Intelligence) => 
                    GetBaseAttribute(CharacterAttribute.Intelligence)
                    + (CurrentLevel - 1) * Constants.RogueIntelligenceIncrease
                    + GetAttributeBoost(CharacterAttribute.Intelligence),
                (CharacterClass.Rogue, CharacterAttribute.Dexterity) => 
                    GetBaseAttribute(CharacterAttribute.Dexterity)
                    + (CurrentLevel - 1) * Constants.RogueDexterityIncrease
                    + GetAttributeBoost(CharacterAttribute.Dexterity),
                (CharacterClass.Rogue, CharacterAttribute.Strength) => 
                    GetBaseAttribute(CharacterAttribute.Strength)
                    + (CurrentLevel - 1) * Constants.RogueStrengthIncrease
                    + GetAttributeBoost(CharacterAttribute.Strength),
                
                (CharacterClass.Warrior, CharacterAttribute.Intelligence) => 
                    GetBaseAttribute(CharacterAttribute.Intelligence)
                    + (CurrentLevel - 1) * Constants.WarriorIntelligenceIncrease
                    + GetAttributeBoost(CharacterAttribute.Intelligence),
                (CharacterClass.Warrior, CharacterAttribute.Dexterity) => 
                    GetBaseAttribute(CharacterAttribute.Dexterity)
                    + (CurrentLevel - 1) * Constants.WarriorDexterityIncrease
                    + GetAttributeBoost(CharacterAttribute.Dexterity),
                (CharacterClass.Warrior, CharacterAttribute.Strength) => 
                    GetBaseAttribute(CharacterAttribute.Strength)
                    + (CurrentLevel - 1) * Constants.WarriorStrengthIncrease
                    + GetAttributeBoost(CharacterAttribute.Strength),
                
                _ => 0
            };
        }
    }
}