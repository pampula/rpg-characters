﻿namespace rpg_characters.Character
{
    public static class NamePicker
    {
        /// <summary>
        /// Character name pick flow. Iterates through all the existing characters and let's the user decide whether they want to name the characters themselves or give then auto-generated names.
        /// </summary>
        public static void PickCharacterNames()
        {
            Console.WriteLine("Welcome to RPG character creator!\nThe program is best experienced in full screen.");

            Console.WriteLine(
                "\nYou have four characters in the game:\nMage\nRanger\nRogue\nWarrior\n\nDo you want to pick names for your characters or generate them automatically?\n\n1.Pick names\n2.Auto-generate");

            bool selected = false;
            bool autogenerate = false;

            while (!selected)
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        autogenerate = false;
                        selected = true;
                        break;
                    case ConsoleKey.D2:
                        autogenerate = true;
                        selected = true;
                        break;
                }

            if (autogenerate)
            {
                foreach (var currentCharacter in CharacterManager.CurrentCharacters)
                {
                    string generatedName = NameGenerator.GenerateName(currentCharacter.CurrentClass);
                    currentCharacter.Name = generatedName;
                }
            }
            else
            {
                for (int i = 0; i < CharacterManager.CurrentCharacters.Count; ++i)
                {
                    rpg_characters.Character.Character currentCharacter = CharacterManager.CurrentCharacters[i];
                    bool nameTypeSelected = false;
                    bool random = false;

                    while (nameTypeSelected == false)
                    {
                        Console.Clear();
                        Console.WriteLine(
                            $"Do you want to choose a name for your {currentCharacter.CurrentClass} or generate a random one?\n\n1.Choose name\n2.Generate a random name\n");
                        switch (Console.ReadKey(true).Key)
                        {
                            case ConsoleKey.D1:
                                Console.WriteLine($"Please insert the name for your {currentCharacter.CurrentClass}");
                                string selectedName = Console.ReadLine();
                                currentCharacter.Name = selectedName;
                                Console.WriteLine(
                                    $"\nYour {currentCharacter.CurrentClass} has been granted the name {currentCharacter.Name}.");
                                nameTypeSelected = true;
                                break;
                            case ConsoleKey.D2:
                                string generatedName = NameGenerator.GenerateName(currentCharacter.CurrentClass);
                                random = true;
                                currentCharacter.Name = generatedName;
                                nameTypeSelected = true;
                                break;
                            default:
                                currentCharacter.Name = null;
                                Console.Clear();
                                Console.WriteLine("Invalid input.");
                                break;
                        }
                    }

                    if (random)
                    {
                        bool nameConfirmed = false;
                        while (!nameConfirmed)
                        {
                            Console.WriteLine(
                                $"Your {currentCharacter.CurrentClass} has been granted the name {currentCharacter.Name}.");
                            Console.WriteLine("Do you want to proceed with this name?\n\n1.Yes\n2.No\n");
                            switch (Console.ReadKey(true).Key)
                            {
                                case ConsoleKey.D1:
                                    nameConfirmed = true;
                                    break;
                                case ConsoleKey.D2:
                                    string generatedName = NameGenerator.GenerateName(currentCharacter.CurrentClass);
                                    currentCharacter.Name = generatedName;
                                    break;
                                default:
                                    Console.Clear();
                                    Console.WriteLine("Invalid input.");
                                    break;
                            }
                        }
                    }

                    Console.Clear();
                    // Console.WriteLine($"{currentCharacter.Name} is ready for action!");
                    currentCharacter.Greet();
                    Thread.Sleep(TimeSpan.FromSeconds(Constants.DialogueDelay));
                }
            }
        }
    }
}