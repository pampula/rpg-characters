﻿namespace rpg_characters.Character
{
    /// <summary>
    /// Manages Character creation and the currently selected character.
    /// </summary>
    public static class CharacterManager
    {
        /// <summary>
        /// Currently selected character.
        /// </summary>
        public static rpg_characters.Character.Character SelectedCharacter { get; set; } = null;

        /// <summary>
        /// A short character description that includes the name, the level and the class of the character.
        /// </summary>
        /// <returns>Character info as a string value.</returns>
        public static string GetCharacterInfoShort()
        {
            return $"{SelectedCharacter.Name} (Level {SelectedCharacter.CurrentLevel} {SelectedCharacter.CurrentClass})";
        }
        
        /// <summary>
        /// List of generated characters.
        /// </summary>
        public static List<rpg_characters.Character.Character> CurrentCharacters { get; private set; } = new List<rpg_characters.Character.Character>(4);
        
        /// <summary>
        /// Method for generating one of each character type.
        /// </summary>
        public static void GenerateCharacters()
        {
            CurrentCharacters.Add(new CharacterClasses.Mage());
            CurrentCharacters.Add(new CharacterClasses.Ranger());
            CurrentCharacters.Add(new CharacterClasses.Rogue());
            CurrentCharacters.Add(new CharacterClasses.Warrior());
        }

        /// <summary>
        /// Switch currently selected character to the next one.
        /// </summary>
        public static void NextCharacter()
        {
            for (int i = 0; i < CurrentCharacters.Count; ++i)
            {
                if (CurrentCharacters[i] == SelectedCharacter)
                {
                    SelectedCharacter = CurrentCharacters[i == CurrentCharacters.Count - 1 ? 0 : (i + 1)];
                    break;
                }
            }
        }
        
        /// <summary>
        /// Switch currently selected character to the previous one.
        /// </summary>
        public static void PreviousCharacter()
        {
            for (int i = 0; i < CurrentCharacters.Count; ++i)
            {
                if (CurrentCharacters[i] == SelectedCharacter)
                {
                    SelectedCharacter = CurrentCharacters[i == 0 ? CurrentCharacters.Count - 1 : (i - 1)];
                    break;
                }
            }
        }
    }
}