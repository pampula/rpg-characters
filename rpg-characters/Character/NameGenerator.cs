﻿namespace rpg_characters.Character
{
    public static class NameGenerator
    {
        /// <summary>
        /// Generates a random name for a character. The generated name is dependent on the character class.
        /// </summary>
        /// <param name="currentClass">The class of the named character.</param>
        /// <returns>A random name for the character.</returns>
        public static string GenerateName(CharacterClass currentClass)
        {
            string name = "";
            var secondName = "";

            switch (currentClass)
            {
                case CharacterClass.Mage:
                    name = $"{Constants.MageNames[new Random().Next(Constants.MageNames.Length)]}";
                    secondName = "";
                    while (secondName == "")
                    {
                        var secondPart = Constants.MageNames[new Random().Next(Constants.MageNames.Length)];
                        secondName = name == secondPart ? "" : secondPart;
                    }
                    name += secondName;
                    break;
                case CharacterClass.Ranger:
                    name = $"{Constants.RangerNames[new Random().Next(Constants.RangerNames.Length)]}";
                    secondName = "";
                    while (secondName == "")
                    {
                        var secondPart = Constants.RangerNames[new Random().Next(Constants.RangerNames.Length)];
                        secondName = name == secondPart ? "" : secondPart;
                    }
                    name += secondName;
                    break;
                case CharacterClass.Rogue:
                    name = $"{Constants.RogueNames[new Random().Next(Constants.RogueNames.Length)]}";
                    secondName = "";
                    while (secondName == "")
                    {
                        var secondPart = Constants.RogueNames[new Random().Next(Constants.RogueNames.Length)];
                        secondName = name == secondPart ? "" : secondPart;
                    }
                    name += secondName;
                    break;
                case CharacterClass.Warrior:
                    name = $"{Constants.WarriorNames[new Random().Next(Constants.WarriorNames.Length)]}";
                    secondName = "";
                    while (secondName == "")
                    {
                        var secondPart = Constants.WarriorNames[new Random().Next(Constants.WarriorNames.Length)];
                        secondName = name == secondPart ? "" : secondPart;
                    }
                    name += secondName;
                    break;
                default:
                    name = "ERROR";
                    break;
            }
            
            return string.Concat(name[0].ToString().ToUpper(), name.AsSpan(1));
        }
    }
}