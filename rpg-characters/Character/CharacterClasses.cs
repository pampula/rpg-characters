﻿using rpg_characters.Items;

namespace rpg_characters.Character
{
    public class CharacterClasses
    {
        /// <summary>
        /// Mage is a character class that gets damage boost for the Intelligence attribute. A Mage can equip Staffs and Wands as weapons and Cloth as armor.
        /// </summary>
        public class Mage : Character
        {
            public override void Greet()
            {
                Console.WriteLine($"{Name}: 'Magical!'");
            }

            public Mage()
            {
                CurrentClass = CharacterClass.Mage;
            }

            protected override List<WeaponType> AllowedWeaponTypes { get; set; } = new()
            {
                WeaponType.Staff,
                WeaponType.Wand
            };

            protected override List<ArmorType> AllowedArmorTypes { get; set; } = new()
            {
                ArmorType.Cloth
            };
        }

        /// <summary>
        /// Ranger is a character class that gets damage boost for the Dexterity attribute. A ranger can equip Bows as weapons and Leather and Mail as armor.
        /// </summary>
        public class Ranger : Character
        {
            public override void Greet()
            {
                Console.WriteLine($"{Name}: 'Let the hunt begin.'");
            }

            public Ranger()
            {
                CurrentClass = CharacterClass.Ranger;
            }

            protected override List<WeaponType> AllowedWeaponTypes { get; set; } = new()
            {
                WeaponType.Bow
            };

            protected override List<ArmorType> AllowedArmorTypes { get; set; } = new()
            {
                ArmorType.Leather,
                ArmorType.Mail
            };
        }

        /// <summary>
        /// Rogue is a character class that gets damage boost for the Dexterity attribute. A rogue can equip Daggers and Swords as weapons and Leather and Mail as armor.
        /// </summary>
        public class Rogue : Character
        {
            public override void Greet()
            {
                Console.WriteLine($"{Name}: 'Better watch your back.'");
            }

            public Rogue()
            {
                CurrentClass = CharacterClass.Rogue;
            }

            protected override List<WeaponType> AllowedWeaponTypes { get; set; } = new()
            {
                WeaponType.Dagger,
                WeaponType.Sword
            };

            protected override List<ArmorType> AllowedArmorTypes { get; set; } = new()
            {
                ArmorType.Leather,
                ArmorType.Mail
            };
        }

        /// <summary>
        /// Warrior is a character class that gets damage boost for the Strength attribute. A warrior can equip Axes, Hammers and Swords as weapons and Mail and Plate as armor.
        /// </summary>
        public class Warrior : Character
        {
            public override void Greet()
            {
                Console.WriteLine($"{Name}: 'Bring it on!'");
            }

            public Warrior()
            {
                CurrentClass = CharacterClass.Warrior;
            }

            protected override List<ArmorType> AllowedArmorTypes { get; set; } = new()
            {
                ArmorType.Mail,
                ArmorType.Plate
            };

            protected override List<WeaponType> AllowedWeaponTypes { get; set; } = new()
            {
                WeaponType.Axe,
                WeaponType.Hammer,
                WeaponType.Sword
            };
        }
    }
}