﻿namespace rpg_characters.Character
{
    /// <summary>
    /// Character class affects how the attributes of the hero and what items they are permitted to equip.
    /// </summary>
    public enum CharacterClass
    {
        Mage,
        Ranger,
        Rogue,
        Warrior
    }

    /// <summary>
    /// The base attributes each character class has. Each character class has a specific character attribute that their damage is based on.
    /// </summary>
    public enum CharacterAttribute
    {
        Strength,
        Dexterity,
        Intelligence
    }
}