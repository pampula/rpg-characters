﻿namespace rpg_characters;

public class RpgExceptions
{
    public class InvalidWeaponException : Exception
    {
        public override string Message => "Not allowed for this class.";
    }
    
    public class InvalidArmorException : Exception
    {
        public override string Message => "Not allowed for this class.";
    }
    
    public class InvalidItemLevelException : Exception
    {
        public override string Message => "Level requirement not met.";
    }
}