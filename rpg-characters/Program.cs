﻿using rpg_characters.Character;
using rpg_characters.Items;
using rpg_characters.Menus;

namespace rpg_characters
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            CharacterManager.GenerateCharacters();
            Thread.Sleep(TimeSpan.FromSeconds(Constants.DialogueDelay));
            NamePicker.PickCharacterNames();
            ItemManager.GenerateItems();
            MenuManager.ShowMenus();
        }
    }
}