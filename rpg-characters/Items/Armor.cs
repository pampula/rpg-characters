﻿using rpg_characters.Character;

namespace rpg_characters.Items
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public int Power { get; }

        public Armor(int identifier, 
            string name, 
            int requiredLevel, 
            ArmorType armorType, 
            ItemSlot itemSlot, 
            CharacterAttribute? attributeBoostType, 
            int? attributeBoostValue)
        {
            Identifier = identifier;
            Name = name;
            RequiredLevel = requiredLevel;
            ArmorType = armorType;
            Slot = itemSlot;
            AttributeBoostType = attributeBoostType;
            AttributeBoostValue = attributeBoostValue;
        }

        /// <summary>
        /// Generates a more flavorful name for each armor piece based on their type and itemslot.
        /// </summary>
        /// <returns>A name for the item as a string value.</returns>
        private string GetGarmentName()
        {
            string name = "";
            switch (Slot, ArmorType)
            {
                case (ItemSlot.Legs, ArmorType.Cloth):
                case (ItemSlot.Legs, ArmorType.Leather):
                    return "Pants";
                case (ItemSlot.Legs, ArmorType.Mail):
                case (ItemSlot.Legs, ArmorType.Plate):
                    return "Trousers";
                case (ItemSlot.Body, ArmorType.Cloth):
                case (ItemSlot.Body, ArmorType.Leather):
                    return "Tunic";
                case (ItemSlot.Body, ArmorType.Mail):
                case (ItemSlot.Body, ArmorType.Plate):
                    return "Jacket";
                case (ItemSlot.Head, ArmorType.Cloth):
                case (ItemSlot.Head, ArmorType.Leather):
                    return "Cap";
                case (ItemSlot.Head, ArmorType.Mail):
                case (ItemSlot.Head, ArmorType.Plate):
                    return "Helmet";
                default:
                    return "Piece";
            }
        }

        /// <summary>
        /// Generates a full display name for an armor.
        /// </summary>
        /// <returns>A display name for an armor.</returns>
        public override string GetFullName()
        {
            return $"{GetRarity()} {ArmorType} {GetGarmentName()}{(AttributeBoostValue > 0 ? $" of {AttributeBoostType}" : "")} ({Slot})";
        }
    }
}