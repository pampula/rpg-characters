﻿using rpg_characters.Character;

namespace rpg_characters.Items
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; }
        public int BaseDamage { get; }
        public float AttacksPerSecond { get; }

        public Weapon(int identifier, string name, int requiredLevel, WeaponType weaponType, int baseDamage,
            float attacksPerSecond, ItemSlot itemSlot, CharacterAttribute? attributeBoostType, int? attributeBoostValue)
        {
            Identifier = identifier;
            Name = name;
            RequiredLevel = requiredLevel;
            WeaponType = weaponType;
            BaseDamage = baseDamage;
            AttacksPerSecond = attacksPerSecond;
            Slot = itemSlot;
            AttributeBoostType = attributeBoostType;
            AttributeBoostValue = attributeBoostValue;
        }

        public float DPS => BaseDamage * AttacksPerSecond;
    
        /// <summary>
        /// Generates a full display name for a weapon.
        /// </summary>
        /// <returns>A display name for a weapon.</returns>
        public override string GetFullName()
        {
            return $"{GetRarity()} {WeaponType}{(AttributeBoostValue > 0 ? $" of {AttributeBoostType}" : "")}";
        }
    }
}