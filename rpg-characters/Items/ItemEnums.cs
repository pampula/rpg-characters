﻿namespace rpg_characters.Items
{
    /// <summary>
    /// Items are divided to armor and weapons.
    /// </summary>
    public enum ItemType
    {
        Weapon,
        Armor
    }

    /// <summary>
    /// Specifies the different weapon types.
    /// </summary>
    public enum WeaponType
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand,
        Count
    }

    /// <summary>
    /// Specifies the different armor types.
    /// </summary>
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate,
        Count
    }

    /// <summary>
    /// The item slot where an armor or a weapon is equipped to.
    /// </summary>
    public enum ItemSlot
    {
        Head,
        Body,
        Legs,
        Weapon
    }
}