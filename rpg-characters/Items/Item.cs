﻿using rpg_characters.Character;

namespace rpg_characters.Items
{
    public abstract class Item
    {
        protected string Name;
        public abstract string GetFullName();
        public int RequiredLevel;
        public CharacterAttribute? AttributeBoostType;
        public int? AttributeBoostValue = 0;
        protected int Identifier;
        public ItemSlot Slot;

        /// <summary>
        /// A rarity based on the required level of the item.
        /// </summary>
        /// <returns>A rarity as a string value.</returns>
        public string GetRarity()
        {
            if (RequiredLevel <= Constants.ItemCommonMaxLevel)
            {
                return "Common";
            }
            else if (RequiredLevel <= Constants.ItemRareMaxLevel)
            {
                return "Rare";
            }
            else if (RequiredLevel <= Constants.ItemEpicMaxLevel)
            {
                return "Epic";
            }
            else
            {
                return "Legendary";
            }
        }
    }
}