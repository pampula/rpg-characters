﻿using rpg_characters.Character;

namespace rpg_characters.Items
{
    public static class ItemManager
    {
        public static readonly List<Weapon> AvailableWeapons = new List<Weapon>();
        public static readonly List<Armor> AvailableArmors = new List<Armor>();
        static readonly Random Rand = new Random();
        private static readonly int GeneratedWeaponCount = 14;
        private static readonly int GeneratedArmorCount = 14;

        /// <summary>
        /// Generate new set of weapons and armor. Can be used to regenerate new items but the currently non-equipped items will be removed then.
        /// </summary>
        public static void GenerateItems(ItemType? itemType = null)
        {
            if (itemType == null || itemType == ItemType.Weapon)
            {
                AvailableWeapons.Clear();
                for (int i = 1; i <= GeneratedWeaponCount; ++i)
                {
                    var thisWeaponRequiredLevel = i + Rand.Next(0,2);
                    var attributeBoost = thisWeaponRequiredLevel > Constants.ItemCommonMaxLevel
                        ? Rand.Next(1, 4) * i
                        : 0;
                    WeaponType weapon = (WeaponType)(Rand.Next(0, Enum.GetNames(typeof(WeaponType)).Length - 1));
                    AvailableWeapons.Add(new Weapon(i, nameof(weapon), thisWeaponRequiredLevel, weapon,
                        Rand.Next(10, 16) * i, Rand.Next(1, 4), ItemSlot.Weapon, GetRandomCharacterAttribute(weapon),
                        attributeBoost));
                }
            }

            if (itemType == null || itemType == ItemType.Armor)
            {
                AvailableArmors.Clear();
                for (int i = 1; i <= GeneratedArmorCount; ++i)
                {
                    var thisArmorRequiredLevel = i + Rand.Next(0,2);
                    var attributeBoost =
                        thisArmorRequiredLevel > Constants.ItemCommonMaxLevel ? Rand.Next(0, 4) * i : 0;
                    ArmorType armor = (ArmorType)(Rand.Next(0, Enum.GetNames(typeof(ArmorType)).Length - 1));
                    AvailableArmors.Add(new Armor(i, nameof(armor), thisArmorRequiredLevel, armor, (ItemSlot)(i % 3),
                        GetRandomCharacterAttribute(armor), attributeBoost));
                }
            }
        }

        /// <summary>
        /// Method for equipping armor. Checks if the armor type is permitted for the selected character class.
        /// </summary>
        /// <param name="armor">Armor type.</param>
        /// <param name="selectedCharacter">The character that is equipping the armor.</param>
        public static void EquipArmor(Armor armor, Character.Character selectedCharacter)
        {
            if (selectedCharacter.EquipArmor(armor))
            {
                AvailableArmors.Remove(armor);
            }
            else
            {
                Console.WriteLine(
                    $"Could not equip level {armor.RequiredLevel} {armor.ArmorType} for level {selectedCharacter.CurrentLevel} {selectedCharacter.CurrentClass}.");
            }
        }

        /// <summary>
        /// Method for equipping weapons. Checks if the weapon type is permitted for the selected character class.
        /// </summary>
        /// <param name="weapon">Weapon type.</param>
        /// <param name="selectedCharacter">The character that is equipping the weapon.</param>
        public static void EquipWeapon(Weapon weapon, Character.Character selectedCharacter)
        {
            if (selectedCharacter.EquipWeapon(ItemSlot.Weapon, weapon))
            {
                AvailableWeapons.Remove(weapon);
            }
            else
            {
                Console.WriteLine(
                    $"Could not equip level {weapon.RequiredLevel} {weapon.WeaponType} for level {selectedCharacter.CurrentLevel} {selectedCharacter.CurrentClass}.");
            }
        }

        /// <summary>
        /// Used for adding a random boosted attribute to an item.
        /// The randomized attribute is restricted by the item type so attributes that would conflict with the character class are not provided.
        /// </summary>
        /// <param name="itemType">Type of item (armor or weapon).</param>
        /// <returns>A random character attribute.</returns>
        private static CharacterAttribute? GetRandomCharacterAttribute<T>(T itemType)
        {
            var rnd = new Random();
            List<CharacterAttribute> allowedAttributes = new List<CharacterAttribute>();

            switch (itemType)
            {
                case ArmorType.Cloth:
                case WeaponType.Staff:
                case WeaponType.Wand:
                    allowedAttributes.Add(CharacterAttribute.Intelligence);
                    break;
                case ArmorType.Leather:
                case WeaponType.Bow:
                case WeaponType.Dagger:
                    allowedAttributes.Add(CharacterAttribute.Dexterity);
                    break;
                case ArmorType.Mail:
                case WeaponType.Sword:
                    allowedAttributes.Add(CharacterAttribute.Dexterity);
                    allowedAttributes.Add(CharacterAttribute.Strength);
                    break;
                case ArmorType.Plate:
                case WeaponType.Axe:
                case WeaponType.Hammer:
                    allowedAttributes.Add(CharacterAttribute.Strength);
                    break;
            }

            return allowedAttributes.Count > 0 ? allowedAttributes[rnd.Next(0, allowedAttributes.Count - 1)] : null;
        }

        /// <summary>
        /// Longest available item name as an integer.
        /// </summary>
        /// <param name="type">Item type, either armor or weapon.</param>
        /// <returns>Longest name length as integer.</returns>
        public static int GetLongestName(ItemType type)
        {
            int longest = 0;
            switch (type)
            {
                case ItemType.Armor:
                    foreach (Armor a in AvailableArmors)
                    {
                        int thisLength = a.GetFullName().Length;
                        if (thisLength > longest)
                        {
                            longest = thisLength;
                        }
                    }
                    break;
                case ItemType.Weapon:
                    foreach (Weapon w in AvailableWeapons)
                    {
                        int thisLength = w.GetFullName().Length;
                        if (thisLength > longest)
                        {
                            longest = thisLength;
                        }
                    }
                    break;
            }

            return longest;
        }

        public static string GetCharacterClassesForItem<T>(T itemType)
        {
            string classes = "";
            switch (itemType)
            {
                case ArmorType.Cloth:
                case WeaponType.Staff:
                case WeaponType.Wand:
                    classes = "Mage";
                    break;
                case ArmorType.Plate:
                case WeaponType.Axe:
                case WeaponType.Hammer:
                    classes = "Warrior";
                    break;
                case ArmorType.Mail:
                    classes = "Ranger, Rogue or Warrior";
                    break;
                case ArmorType.Leather:
                    classes = "Ranger or Rogue";
                    break;
                case WeaponType.Sword:
                    classes = "Rogue or Warrior";
                    break;
                case WeaponType.Bow:
                    classes = "Ranger";
                    break;
                case WeaponType.Dagger:
                    classes = "Rogue";
                    break;
            }
            
            return classes;
        }
    }
}